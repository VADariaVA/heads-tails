const Sugar = require('sugar');
const readline = require('readline');
const fs = require('fs');
const file = 'game.txt';

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.question('1 or 2? ', (answer) => {
	var num = Sugar.Number.random(1,2);
	if (answer == num) {
		console.log("Yes, it's: ", answer);
	} else { console.log("Lol, nope! Answer is: ", num); }

	fs.appendFile(file, 'Your answer: ' + answer + ' and result is: ' + num + '\n', (err) => {
		if (err) throw err;
	});
	rl.close();
});
